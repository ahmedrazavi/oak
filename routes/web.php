<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/artisan', 'WebArtisanController@index');
Route::post('/artisan', 'WebArtisanController@run')->name('artisan.run');

Route::resource('contact', 'ContactController');
Route::resource('guardian', 'GuardianController');
Route::resource('course', 'CourseController');
Route::resource('subject', 'SubjectController');
Route::resource('batch', 'BatchController');
Route::resource('teacher', 'TeacherController');
Route::resource('student', 'StudentController');
Route::resource('room', 'RoomController');
Route::resource('classes', 'ClassesController');
Route::resource('applicant', 'ApplicantController');

Route::get('academic', 'AcademicController@index')->name('academic.index');
Route::get('academic/classes', 'AcademicController@classes')->name('academic.classes');
Route::get('academic/courses', 'AcademicController@courses')->name('academic.courses');
Route::post('search/students', 'StudentController@search')->name('student.search');

Route::get('financial', 'AccountController@index')->name('financial.index');
Route::resource('account', 'AccountController');
Route::resource('transaction', 'TransactionController');

