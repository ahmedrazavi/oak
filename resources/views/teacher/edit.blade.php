@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('teacher.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Edit Teacher</div>
        <form method="POST" action="{{ route('teacher.update', $teacher) }}">
          @csrf @method('PUT')

          <x-input type="text" name="name" :editName="$teacher->name" />
          <x-input type="text" name="education" :editName="$teacher->education" />
          <x-input type="email" name="email" :editName="$teacher->email" />
          <x-input type="phone" name="phone" :editName="$teacher->phone" />
          <x-input type="text" name="address" :editName="$teacher->address" />

          <button type="submit" class="btn btn-primary">{{ __('Update Teacher') }}</button>
          <a href="#" onclick="document.getElementById('deleteForm').submit()" class="btn btn-outline-danger">Delete</a>
        </form>
        <form id="deleteForm" action="{{route('teacher.destroy', $teacher)}}" method="post">
          @csrf @method('DELETE')
        </form>
      </div>
    </div>
</div>
@endsection
