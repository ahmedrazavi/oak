@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('teacher.aside')
      </nav>
    </div>
</div>
<div class="col-md-9">
    @include('includes.message')

    <div class="card">
        <div class="card-body pb-0">
          <div class="d-flex justify-content-between align-items-start">
            <div class="card-title h3">Teachers</div>
            <a href="{{ route('teacher.create') }}" class="btn btn-sm btn-primary">New</a>
          </div>
        </div>

        <table class="table">
        
          <tr>
            <th>Name</th>
            <th>Education</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
          </tr>

          @forelse($teachers as $teacher)

            <tr>
              <td><a href="{{ route('teacher.show', $teacher->id) }}">{{$teacher->name}}</a></td>
              <td>{{$teacher->education}}</td>
              <td>{{$teacher->email}}</td>
              <td>{{$teacher->phone}}</td>
              <td>{{$teacher->address}}</td>
            </tr>

          @empty
          
            <tr>
              <td colspan="4">No teachers found</td>
            </tr>

          @endforelse

        </table>
    </div>
</div>
@endsection
