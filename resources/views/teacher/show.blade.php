@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('teacher.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body pb-0">
        <div class="card-title h3">{{ $teacher->name }}</div>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item d-flex justify-content-between align-items-center">
        ID
        <span>#{{ $teacher->id }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Name
        <span>{{ $teacher->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Education
        <span>{{ $teacher->education }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Email
        <span>{{ $teacher->email }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Phone
        <span>{{ $teacher->phone }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Address
        <span>{{ $teacher->address }}</span>
      </li>
    </ul>
    <div class="m-3 d-flex">
      <form action="{{route('teacher.destroy', $teacher)}}" method="post">
        @csrf @method('DELETE')
        <a href="{{route('teacher.edit',$teacher)}}" class="btn btn-primary">Edit Teacher</a>
        <button type="submit" class="btn btn-outline-danger">Delete</button>
      </form>
    </div>
    
</div>
@endsection
