<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('includes.head')
<body>
    <div id="app">
        @include('includes.navigation')

        <main class="py-4">
          <div class="container">
              <div class="row">
                @yield('content')
              </div>
          </div>
        </main>
    </div>
</body>
</html>
