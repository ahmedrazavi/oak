<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('includes.head')
<body>
    <div id="app">
        @include('includes.navigation')

        <main class="py-4">
          <div class="container">
              <div class="row">
                <div class="col-md-3">
                  <div class="card">
                      <nav class="nav flex-column">
                        <a class="nav-link" href="{{ route('academic.index') }}">Overview</a>
                        <a class="nav-link" href="{{ route('account.index') }}">Chart Of Accounts</a>
                        <a class="nav-link" href="{{ route('transaction.index') }}">Transactions</a>
                        <a class="nav-link" href="{{ route('academic.courses') }}">Account Statements</a>
                        <a class="nav-link" href="{{ route('student.create') }}">Balance Sheet</a>
                        <a class="nav-link" href="{{ route('teacher.index') }}">Income Statement</a>
                        <a class="nav-link" href="{{ route('room.index') }}">Cashflow Statement</a>
                      </nav>
                    </div>
                </div>
                <div class="col-md-9">
                  @yield('content')
                </div>
              </div>
          </div>
        </main>
    </div>
</body>
</html>
