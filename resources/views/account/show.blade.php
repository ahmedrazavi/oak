@extends('layouts.financial')

@section('content')
@include('includes.message')

<div class="card">

  <div class="card-body pb-0">
    <div class="d-flex justify-content-between align-items-start">
      <div class="card-title h3">{{ $account->name }}</div>
      <a href="{{ route('transaction.create') }}" class="btn btn-sm btn-primary">+ Filter</a>
    </div>
  </div>

  <table class="table table-striped mb-0 table-bordered">
  
    <tr class="text-center">
      <th>ID</th>
      <th>Date</th>
      <th>Account</th>
      <th>Debit</th>
      <th>Credit</th>
      <th>Balance</th>
    </tr>

    @php $balance = 0 @endphp
    @forelse($account->transactions as $transaction)
    @php $balance += $transaction->amount @endphp
      <tr>
        <td class="text-center"><a href="{{route('transaction.show',$transaction->id)}}">#{{ $transaction->transaction->id }}</a></td>
        <td>{{ $transaction->transaction->date }}</td>
        <td>
            @foreach($transaction->sources as $source)
              <a href="{{route('account.show', $source->account->id)}}">
                {{ $source->account->name }}
              </a>
              @if($loop->count > 1) 
              <a href="{{route('account.show', $transaction->transaction->id)}}">
                ...
              </a>
              @break
              @endif
            @endforeach
        </td>
        <td class="text-right">{{ $transaction->amount > 0 ? $transaction->amount : '' }}</td>
        <td class="text-right">{{ $transaction->amount < 0 ? $transaction->amount : '' }}</td>
        <td class="text-right @if($balance < 0) text-danger @endif">{{ $balance }}</td>
      </tr>
    @empty
      <tr>
        <td colspan="40">No transactions found</td>
      </tr>
    @endforelse

  </table>
  
</div>

@endsection
