@extends('layouts.financial')

@section('content')
@include('includes.message')

<div class="card">

  <div class="card-body pb-0">
    <div class="d-flex justify-content-between align-items-start">
      <div class="card-title h3">Chart Of Accounts</div>
      <a href="{{ route('account.create') }}" class="btn btn-sm btn-primary">+ Create</a>
    </div>
  </div>

  <table class="table table-striped">
  
    <tr>
      <th>ID</th>
      <th>Nature</th>
      <th>Category</th>
      <th>Control</th>
      <th>Name</th>
      <th>Balance</th>
    </tr>

    @forelse($accounts as $account)

      <tr>
        <td>{{ $account->id }}</td>
        <td>{{ $account->nature }}</td>
        <td>{{ $account->category }}</td>
        <td>{{ $account->control }}</td>
        <td><a href="{{ route('account.show', $account->id) }}">{{ $account->name }}</a></td>
        <td>{{ $account->balance }}</td>
      </tr>

    @empty

      <tr>
        <td colspan="40">No accounts found</td>
      </tr>

    @endforelse

  </table>
  
</div>

@endsection
