@extends('layouts.financial')

@section('content')
@include('includes.message')
<div class="card">
  <div class="card-body">
    <div class="card-title h3">Create Account</div>
    <form method="POST" action="{{ route('account.store') }}">
      @csrf

      <div class="form-group">
        <label for="nature" class="form-label">Nature:</label>

        <select onchange="
          switch(this.value){
            case 'Asset':
              var accounts = ['Current Asset', 'Fixed Asset']
              break;
            case 'Liability':
              var accounts = ['Short Term Liability', 'Long Term Liability']
              break;
            case 'Capital':
              var accounts = ['Owner\'s Equity']
              break;
            case 'Expense':
              var accounts = ['Operating Expense', 'Non-operating Expense']
              break;
            case 'Revenue':
              var accounts = ['Operating Revenue', 'Other Revenue']
              break;}
          document.getElementById('category').innerHTML = '<option selected disabled>Please Select</option>'
          accounts.forEach(element => document.getElementById('category').innerHTML += `<option>${element}</option>`)" class="form-control @error('nature') is-invalid @enderror" name="nature" id="nature">
          <option selected disabled>Please Select</option>
          <option>Asset</option>
          <option>Liability</option>
          <option>Capital</option>
          <option>Expense</option>
          <option>Revenue</option>
        </select>

        @error('nature')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="category" class="form-label">Category:</label>

        <select onchange="
          switch(this.value){
            case 'Current Asset':
              var accounts = ['Cash and Banks', 'Undeposited Funds', 'Accounts Receivables', 'Prepaid Expense', 'Inventory']
              break;
            case 'Fixed Asset':
              var accounts = ['Buildings', 'Equipment', 'Furniture', 'Machinery', 'Land', 'Vehicles']
              break;
            case 'Short Term Liability':
              var accounts = ['Accounts Payable', 'Bills Payable', 'Accrued Expenses']
              break;
            case 'Long Term Liability':
              var accounts = ['Long Term Loan']
              break;
            case 'Owner\'s Equity':
              var accounts = ['Opening Balance Equity', 'Retained Earning']
              break;
            case 'Operating Expense':
              var accounts = ['Advertising', 'Office Expenses', 'Utilities Expenses']
              break;
            case 'Non-operating Expense':
              var accounts = ['Operating Revenue', 'Other Revenue', 'Ask My Accountant']
              break;
            case 'Operating Revenue':
              var accounts = ['Sales']
              break;
            case 'Other Revenue':
              var accounts = ['Sales on Disposal of an Asset']
              break;}
          document.getElementById('control').innerHTML = '<option selected disabled>Please Select</option>'
          accounts.forEach(element => document.getElementById('control').innerHTML += `<option>${element}</option>`)
        " class="form-control @error('category') is-invalid @enderror" name="category" id="category">
        </select>

        @error('category')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>

      <div class="form-group">
        <label for="control" class="form-label">Control:</label>

        <select class="form-control @error('control') is-invalid @enderror" name="control" id="control">

        </select>

        @error('control')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>

      <x-input type="text" name="name" />

      <button type="submit" class="btn btn-primary">{{ __('Save Account') }}</button>
    </form>
  </div>
</div>
@endsection
