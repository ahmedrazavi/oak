<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Web Artisan</title>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #282828;
                color: #00FF66;
                font-size: 16px;
                text-align: center;
                margin: 0;
            }

            .container {
              display: grid;
              grid-template-rows:  30vh 70vh
            }

            .input {
              margin-top: auto;
              margin-bottom: auto;
              color: #00FF66;
              font-size: 6em;
              font-family: 'Lucida Console', 'Monaco', monospace;
            }

            .output {
              text-align:left;
              font-size:1em; 
              padding-top:12px;
              margin-left: auto;
              margin-right: auto;
            }

            .input-field {
              background-color: #282828;
              color: #00FF66;
              font-size: 84px;
              font-family: 'Lucida Console', 'Monaco', monospace;
              border-width: 0;
            }
            .input-field:focus {
              outline: none;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="input">
              <form action="{{ route('artisan.run') }}" method="post">
                @csrf
                :<input name="command" id="command" class="input-field" type="text">
              </form>
            </div>
            <div class="output">
              <pre id="output">{{ $artisan_output ?? '' }}</pre>
            </div>
        </div>
        <script>
          console.log(document.getElementById("output").innerText)

          document.addEventListener("keyup", function(event) {
            if(event.which == 32){
              document.getElementById("command").focus()
            }
          })
        </script>
    </body>
</html>
