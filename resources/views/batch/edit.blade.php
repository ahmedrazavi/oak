@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('batch.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Edit batch</div>
        <form method="POST" action="{{ route('batch.update', $batch) }}">
          @csrf @method('PUT')

          <div class="form-group">
            <label for="course" class="form-label">Course:</label>

            <select class="form-control @error('course') is-invalid @enderror" name="course" id="course">
              @forelse($courses as $course)
                <option value="{{ $course->id }}" @if($batch->course_id == $course->id) selected @endif>{{ $course->name }}</option>
              @empty
                <option selected disabled>Please add course first</option>
              @endforelse
            </select>

            @error('course')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>

          <x-input type="text" name="session" :editName="$batch->session" />

          <button type="submit" class="btn btn-primary">{{ __('Update Batch') }}</button>
          <a href="#" onclick="document.getElementById('deleteForm').submit(); return false;" class="btn btn-outline-danger">Delete</a>
        </form>
        <form id="deleteForm" action="{{route('batch.destroy', $batch)}}" method="post">
          @csrf @method('DELETE')
        </form>
      </div>
    </div>
</div>
@endsection
