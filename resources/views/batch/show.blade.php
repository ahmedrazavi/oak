@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('batch.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body pb-0">
        <div class="card-title h3">{{ $batch->course->name }} - {{ $batch->session }}</div>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item d-flex justify-content-between align-items-center">
        ID
        <span>#{{ $batch->id }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Name
        <span>{{ $batch->course->name }} - {{ $batch->session }}</span>
      </li>
    </ul>
    <div class="m-3 d-flex">
      <form action="{{route('batch.destroy', $batch)}}" method="post">
        @csrf @method('DELETE')
        <a href="{{route('batch.edit',$batch)}}" class="btn btn-primary">Edit Batch</a>
        <button type="submit" class="btn btn-outline-danger">Delete</button>
      </form>
    </div>
    
</div>
@endsection
