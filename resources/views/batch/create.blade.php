@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('batch.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Create Batch</div>
        <form method="POST" action="{{ route('batch.store') }}">
          @csrf

          <div class="form-group">
            <label for="course" class="form-label">Course:</label>

            <select class="form-control @error('course') is-invalid @enderror" name="course" id="course">
              @forelse($courses as $course)
                <option value="{{ $course->id }}">{{ $course->name }}</option>
              @empty
                <option selected disabled>Please add course first</option>
              @endforelse
            </select>

            @error('course')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>

          <x-input type="text" name="session" />

          <button type="submit" class="btn btn-primary">{{ __('Save Batch') }}</button>
        </form>
      </div>
    </div>
</div>
@endsection
