@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('batch.aside')
      </nav>
    </div>
</div>
<div class="col-md-9">
    @include('includes.message')

    <div class="card">
        <div class="card-body pb-0">
          <div class="d-flex justify-content-between align-items-start">
            <div class="card-title h3">Batches</div>
            <a href="{{ route('batch.create') }}" class="btn btn-sm btn-primary">New</a>
          </div>
        </div>

        <table class="table">
        
          <tr>
            <th>ID</th>
            <th>Course</th>
            <th>Batch</th>
          </tr>

          @forelse($batches as $batch)

            <tr>
              <td><a href="{{ route('batch.show',$batch) }}">{{ $batch->id }}</a></td>
              <td>{{ $batch->course->name }}</td>
              <td>{{ $batch->session }}</td>
            </tr>

          @empty
          
            <tr>
              <td colspan="4">No batches found</td>
            </tr>

          @endforelse

        </table>
    </div>
</div>
@endsection
