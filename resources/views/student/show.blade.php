@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('student.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body pb-0">
        <div class="card-title h3">{{ $student->name }}</div>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item d-flex justify-content-between align-items-center">
        ID
        <span>#{{ $student->id }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Name
        <span>{{ $student->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Gender
        <span>{{ ucfirst($student->gender) }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Batch
        <span>{{ $student->batch()->exists() ? $student->batch->course->name."-".$student->batch->session : '' }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Guardian
        <span>{{ $student->guardian()->exists() ? $student->guardian->name : '' }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Email
        <span>{{ $student->email }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Phone
        <span>{{ $student->phone }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Address
        <span>{{ $student->address }}</span>
      </li>
    </ul>
    <div class="m-3 d-flex">
      <form action="{{route('student.destroy', $student)}}" method="post">
        @csrf @method('DELETE')
        <a href="{{route('student.edit',$student)}}" class="btn btn-primary">Edit Student</a>
        <button type="submit" class="btn btn-outline-danger">Delete</button>
      </form>
    </div>
    
</div>
@endsection
