@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('student.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Create Student</div>
        
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="student-tab" data-toggle="tab" href="#student" role="tab" aria-controls="student" aria-selected="true">Student</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="guardian-tab" data-toggle="tab" href="#guardian" role="tab" aria-controls="guardian" aria-selected="false">Guardian</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="batch-tab" data-toggle="tab" href="#batch" role="tab" aria-controls="batch" aria-selected="false">Batch</a>
          </li>
        </ul>
        
          <form method="POST" action="{{ route('student.store') }}">
            @csrf
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active my-4" id="student" role="tabpanel" aria-labelledby="student-tab">

                <x-input type="text" name="name" />
                <x-input type="email" name="email" />
                <x-input type="phone" name="phone" />
                <x-input type="text" name="address" />

                <div class="gender-radio-select">
                  <label class="form-label">Gender:</label>
                  <div class="form-group">
                    <div class="custom-control custom-radio custom-control-inline">
                      <input class="custom-control-input" type="radio" name="gender" id="female" value="female">
                      <label class="custom-control-label" for="female">Female</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input class="custom-control-input" type="radio" name="gender" id="male" value="male">
                      <label class="custom-control-label" for="male">Male</label>
                    </div>
                  </div>
                </div>

              </div>

              <!-- Guardian Tab -->
              <div class="tab-pane fade my-4" id="guardian" role="tabpanel" aria-labelledby="guardian-tab">
                
                <!-- <x-input type="text" name="guardian_name" />
                <x-input type="email" name="guardian_email" />
                <x-input type="phone" name="guardian_phone" />
                <x-input type="text" name="guardian_address" /> -->
                <div class="form-group">
                  <label for="course" class="form-label">Guardian:</label>
                  <select class="form-control @error('guardian') is-invalid @enderror" name="guardian" id="guardian">
                    <option selected disabled>Please Select</option>
                    @forelse($guardians as $guardian)
                      <option value="{{ $guardian->id }}">{{ $guardian->name }}</option>
                    @empty
                      <option selected disabled>Please add guardian first</option>
                    @endforelse
                  </select>
                  @error('guardian')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>

              <!-- Batch Tab -->
              <div class="tab-pane fade my-4" id="batch" role="tabpanel" aria-labelledby="batch-tab">
                <div class="form-group">
                  <label for="course" class="form-label">Batch:</label>
                  <select class="form-control @error('batch') is-invalid @enderror" name="batch" id="batch">
                    <option selected disabled>Please Select</option>
                    @forelse($batches as $batch)
                      <option value="{{ $batch->id }}">{{ $batch->course->name }}-{{ $batch->session }}</option>
                    @empty
                      <option selected disabled>Please add batch first</option>
                    @endforelse
                  </select>
                  @error('batch')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>

            </div>
            <button type="submit" class="btn btn-primary">{{ __('Save Student') }}</button>
          </form>

      </div>
    </div>
</div>
@endsection
