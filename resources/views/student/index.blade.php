@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('student.aside')
      </nav>
    </div>
</div>
<div class="col-md-9">
    @include('includes.message')

    <div class="card">
        <div class="card-body pb-0">
          <div class="d-flex justify-content-between align-items-start">
            <div class="card-title h3">Student</div>
            <div>
              <a href="{{ route('student.create') }}" class="btn btn-sm btn-primary">Add</a>
              <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#exampleModal">Search</a>
            </div>
          </div>
        </div>

        <table class="table">
        
          <tr>
            <th>Name</th>
            <th>Guardian</th>
            <th>Gender</th>
            <th>Batch</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
          </tr>

          @forelse($students as $student)

            <tr>
              <td><a href="{{ route('student.show', $student->id) }}">{{$student->name}}</a></td>
              <td>@if($student->guardian()->exists())<a href="{{ route('guardian.show', $student->guardian->id) }}">{{ $student->guardian->name }}</a>@endif</td>
              <td>{{$student->gender}}</td>
              <td>@if($student->batch()->exists())<a href="{{ route('batch.show', $student->batch->id) }}">{{ $student->batch->course->name."-".$student->batch->session }}</a>@endif</td>
              <td>{{$student->email}}</td>
              <td>{{$student->phone}}</td>
              <td>{{$student->address}}</td>
            </tr>

          @empty
          
            <tr>
              <td colspan="7">No students found</td>
            </tr>

          @endforelse

        </table>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Search</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('student.search') }}" method="post">
      @csrf
      <div class="modal-body">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="id-search-tab" href="#id-search" data-toggle="tab" href="#id-search" role="tab" aria-controls="id" aria-selected="true">By ID</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="batch-search-tab" href="#batch-search" data-toggle="tab" href="#batch-search" role="tab" aria-controls="batch" aria-selected="true">By Batch</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="id-search" role="tabpanel" aria-labelledby="id-search">
            <div class="py-3">
              <x-input type="text" name="id" />
            </div>
          </div>
          <div class="tab-pane fade" id="batch-search" role="tabpanel" aria-labelledby="batch-search">
            <div class="py-3">
              <x-select name="batch" :records="$batches" field="name" />
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Search</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
