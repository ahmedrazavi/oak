@if($errors->any())
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    Whoops! There were some errors
    <details>
      <summary>Show Errors</summary>
      <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
      </ul>
    </details>

    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif