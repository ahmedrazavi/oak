@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('guardian.aside')
      </nav>
    </div>
</div>
<div class="col-md-9">
    @include('includes.message')

    <div class="card">
        <div class="card-body pb-0">
          <div class="card-title h3">Guardians</div>
        </div>

        <table class="table">
        
          <tr>
            <th>Name</th>
            <th>Occupation</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
          </tr>

          @forelse($guardians as $guardian)

            <tr>
              <td><a href="{{ route('guardian.show', $guardian->id) }}">{{$guardian->name}}</a></td>
              <td>{{$guardian->occupation}}</td>
              <td>{{$guardian->email}}</td>
              <td>{{$guardian->phone}}</td>
              <td>{{$guardian->address}}</td>
            </tr>

          @empty
          
            <tr>
              <td colspan="4">No guardians found</td>
            </tr>

          @endforelse

        </table>
    </div>
</div>
@endsection
