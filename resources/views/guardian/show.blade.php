@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('guardian.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body pb-0">
        <div class="card-title h3">{{ $guardian->name }}</div>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item d-flex justify-content-between align-items-center">
        ID
        <span>#{{ $guardian->id }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Name
        <span>{{ $guardian->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Occupation
        <span>{{ $guardian->occupation }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Email
        <span>{{ $guardian->email }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Phone
        <span>{{ $guardian->phone }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Address
        <span>{{ $guardian->address }}</span>
      </li>
    </ul>
    <div class="m-3 d-flex">
      <form action="{{route('guardian.destroy', $guardian)}}" method="post">
        @csrf @method('DELETE')
        <a href="{{route('guardian.edit',$guardian)}}" class="btn btn-primary">Edit guardian</a>
        <button type="submit" class="btn btn-outline-danger">Delete</button>
      </form>
    </div>
    
</div>
@endsection
