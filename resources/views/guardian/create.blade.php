@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('guardian.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Create Guardian</div>
        <form method="POST" action="{{ route('guardian.store') }}">
          @csrf

          <x-input type="text" name="name" />
          <x-input type="text" name="occupation" />
          <x-input type="email" name="email" />
          <x-input type="phone" name="phone" />
          <x-input type="text" name="address" />

          <button type="submit" class="btn btn-primary">{{ __('Save Guardian') }}</button>
        </form>
      </div>
    </div>
</div>
@endsection
