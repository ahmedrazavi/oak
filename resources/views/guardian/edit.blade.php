@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('guardian.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Edit Guardian</div>
        <form method="POST" action="{{ route('guardian.update', $guardian) }}">
          @csrf @method('PUT')

          <x-input type="text" name="name" :editName="$guardian->name" />
          <x-input type="text" name="occupation" :editName="$guardian->occupation" />
          <x-input type="email" name="email" :editName="$guardian->email" />
          <x-input type="phone" name="phone" :editName="$guardian->phone" />
          <x-input type="text" name="address" :editName="$guardian->address" />

          <button type="submit" class="btn btn-primary">{{ __('Update Guardian') }}</button>
          <a href="#" onclick="document.getElementById('deleteForm').submit()" class="btn btn-outline-danger">Delete</a>
        </form>
        <form id="deleteForm" action="{{route('guardian.destroy', $guardian)}}" method="post">
          @csrf @method('DELETE')
        </form>
      </div>
    </div>
</div>
@endsection
