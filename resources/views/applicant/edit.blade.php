@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('academic.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Edit Applicant</div>
        <form method="POST" action="{{ route('applicant.update', $applicant) }}">
          @csrf @method('PUT')

          <div class="form-group">
            <label for="status" class="form-label">Status:</label>
            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status">
              <option value="open">OPEN</option>
              <option value="contacted">CONTACTED</option>
            </select>
            @error('status')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <button type="submit" class="btn btn-primary">{{ __('Update applicant') }}</button>
          <a href="#" onclick="document.getElementById('deleteForm').submit(); return false;" class="btn btn-outline-danger">Delete</a>
        </form>
        <form id="deleteForm" action="{{route('applicant.destroy', $applicant)}}" method="post">
          @csrf @method('DELETE')
        </form>
      </div>
    </div>
</div>
@endsection
