@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center">
<div class="col-md-8">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Apply Online</div>
        <form method="POST" action="{{ route('applicant.store') }}">
          @csrf

          <x-input type="text" name="name" />
          <x-input type="text" name="phone" />

          <div class="row">
            <div class="col-md-6">
              <x-select name="programme" :records="$courses" field="name" />
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="session" class="form-label">Session:</label>
                <select class="form-control @error('session') is-invalid @enderror" name="session" id="session">
                  <option value="morning">Morning</option>
                  <option value="evening">Evening</option>
                </select>
                @error('session')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>
          <img src="" onerror="{document.querySelectorAll('.form-label')[2].innerText = 'Program Interested in.'}" />

          <button type="submit" class="btn btn-primary">{{ __('Submit Form') }}</button>
        </form>
      </div>
    </div>
</div>
</div>
</div>
@endsection
