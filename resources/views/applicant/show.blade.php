@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('academic.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body pb-0">
        <div class="card-title h3">{{ $applicant->name }}</div>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item d-flex justify-content-between align-items-center">
        ID
        <span>#{{ $applicant->id }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Name
        <span>{{ $applicant->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Phone
        <span>{{ $applicant->phone }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Programe
        <span>{{ $applicant->course->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Session
        <span class="text-capitalize">{{ $applicant->message }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Status
        <span class="text-uppercase">{{ $applicant->status }}</span>
      </li>
    </ul>
    <div class="m-3 d-flex">
      <form action="{{route('applicant.destroy', $applicant)}}" method="post">
        @csrf @method('DELETE')
        <a href="{{route('applicant.edit',$applicant)}}" class="btn btn-primary">Edit applicant</a>
        <button type="submit" class="btn btn-outline-danger">Delete</button>
      </form>
    </div>
    
</div>
@endsection
