@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('academic.aside')
      </nav>
    </div>
</div>
<div class="col-md-9">
    @include('includes.message')
    <div class="card">
        <div class="card-body pb-0">
          <div class="card-title h3">Classess</div>
        </div>

        <table class="table">
        
          <tr>
            <th>ID</th>
            <th>Batch</th>
            <th>Teacher</th>
            <th>Subject</th>
            <th>Room</th>
            <th>Date</th>
            <th>Time</th>            
            <th>Hours</th>
            <th>Status</th>
          </tr>

          @forelse($classes as $classes)

            <tr>
              <td><a href="{{route('classes.show', $classes->id)}}">#{{$classes->id}}</a></td>
              <td>{{$classes->batch->name}}</td>
              <td>{{$classes->teacher->name}}</td>
              <td>{{$classes->subject->name}}</td>
              <td>{{$classes->room->name}}</td>
              <td>{{date('d M Y', strtotime($classes->timming))}}</td>
              <td>{{date('h:i A', strtotime($classes->timming))}}</td>              
              <td>{{$classes->hours}}</td>
              <td class="text-uppercase">{{$classes->status}}</td>
            </tr>

          @empty
          
            <tr>
              <td colspan="40">No classes found</td>
            </tr>

          @endforelse

        </table>
    </div>
</div>
@endsection
