<a class="nav-link" href="{{ route('academic.index') }}">Overview</a>
<a class="nav-link" href="{{ route('applicant.index') }}">Applicants</a>
<a class="nav-link" href="{{ route('classes.index') }}">Classes Schedule</a>
<a class="nav-link" href="{{ route('student.index') }}">Student Data</a>
<a class="nav-link" href="{{ route('batch.index') }}">Batch Management</a>
<a class="nav-link" href="{{ route('academic.courses') }}">Courses & Subjects</a>
<a class="nav-link" href="{{ route('student.create') }}">New Registrations</a>
<a class="nav-link" href="{{ route('teacher.index') }}">Teachers</a>
<a class="nav-link" href="{{ route('room.index') }}">Classrooms</a>
