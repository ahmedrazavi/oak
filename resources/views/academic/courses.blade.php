@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('academic.aside')
      </nav>
    </div>
</div>
<div class="col-md-9">
    @include('includes.message')
    <div class="card">
      <div class="card-body pb-0">
        <div class="d-flex justify-content-between align-items-start">
          <div class="card-title h3">Courses and Subjects</div>
          <div>
            <a href="{{ route('course.create') }}" class="btn btn-sm btn-primary">New Course</a>
            <a href="{{ route('subject.create') }}" class="btn btn-sm btn-primary">New Subject</a>
          </div>
        </div>
      </div>
    </div>
    <div class="row my-3">
      @forelse($courses as $course)
        <div class="col-md-4">
          <div class="card">
            <div class="card-body pb-0  border-bottom">
              <div class="d-flex justify-content-between align-items-start">
                <div class="card-title h3">{{ $course->name }}</div>
                <a href="{{ route('course.show', $course->id) }}" class="btn btn-sm btn-outline-primary">Edit</a>
              </div>
            </div>
            <div class="list-group list-group-flush">
              @forelse($course->subjects as $subject)
                <a href="{{route('subject.show',$subject->id)}}" class="list-group-item list-group-item-action">{{ $subject->name }}</a>
              @empty
              @endforelse
            </div>
          </div>
        </div>
      @empty
        No Courses
      @endforelse
    </div>
</div>
@endsection
