@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('academic.aside')
      </nav>
    </div>
</div>
<div class="col-md-9">
    @include('includes.message')
    <div class="card">
        <div class="card-body pb-0">
          <div class="card-title h3">Applicants</div>
        </div>

        <table class="table">
        
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Program</th>
            <th>Session</th>
            <th>Date</th>
            <th>Status</th>
          </tr>

          @forelse($applicants as $applicant)

            <tr>
              <td><a href="{{ route('applicant.show', $applicant->id) }}">{{$applicant->id}}</a></td>
              <td>{{$applicant->name}}</td>
              <td>{{$applicant->phone}}</td>
              <td>{{$applicant->course->name}}</td>
              <td class="text-capitalize">{{$applicant->message}}</td>
              <td>{{ date("d M Y",strtotime($applicant->created_at))}}</td>
              <td class="text-uppercase">{{$applicant->status}}</td>
            </tr>

          @empty
          
            <tr>
              <td colspan="40">No applicants found</td>
            </tr>

          @endforelse

        </table>
    </div>
</div>
@endsection
