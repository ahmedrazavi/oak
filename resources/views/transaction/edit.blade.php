@extends('layouts.financial')

@section('content')
@include('includes.message')
<div class="card">
  <div class="card-body">
    <div class="card-title h3">Create Account</div>
    <form method="POST" action="{{ route('account.update', $account->id) }}">
      @csrf @method('PUT')

      <x-input type="text" name="nature" :editName="$account->nature" />
      <x-input type="text" name="category" :editName="$account->category" />
      <x-input type="text" name="control" :editName="$account->control" />
      <x-input type="text" name="name" :editName="$account->name" />

      <button type="submit" class="btn btn-primary">{{ __('Edit Account') }}</button>
      <a href="#" onclick="document.getElementById('deleteForm').submit(); return false;" class="btn btn-outline-danger">Delete</a>
    </form>
    <form id="deleteForm" action="{{route('account.destroy', $account)}}" method="post">
      @csrf @method('DELETE')
    </form>
  </div>
</div>
@endsection
