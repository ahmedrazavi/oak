@extends('layouts.financial')

@section('content')
@include('includes.message')

<div class="card">

  <div class="card-body pb-0">
    <div class="d-flex justify-content-between align-items-start">
      <div class="card-title h3">Select Voucher</div>
      <!-- <a href="{{ route('transaction.create') }}" class="btn btn-sm btn-primary">+ Create</a> -->
    </div>
  </div>

  <table class="table mb-0">
  
    <thead>
      <tr>
        <th>Voucher Name</th>
        <th>Description</th>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td><a href="">Cash Received</a></td>
      </tr>
    </tbody>

  </table>
  
</div>

@endsection
