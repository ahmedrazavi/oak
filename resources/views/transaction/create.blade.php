@extends('layouts.financial')

@section('content')
@include('includes.message')
@include('includes.errors')
<div class="card">
  <div class="card-body">
    <div class="card-title h3">Create Transaction</div>
    <form method="POST" action="{{ route('transaction.store') }}">
      @csrf

      <x-input type="date" name="date" :editName="date('Y-m-d')" />

      <div class="form-group">
        <div class="row">
          <label class="col-md-8">Accounts:</label>
          <label class="col-md-2 text-center">Debit</label>
          <label class="col-md-2 text-center">Credit</label>
        </div>
        <div class="row mt-1" v-for="item in items">
          <div class="col-md-8 pr-1">
            <select class="form-control form-control-lg @error('account') is-invalid @enderror" name="account[]">
              <option selected disabled>Please Select</option>
              @forelse($accounts as $account)
                  <option value="{{ $account->id }}">{{ $account->name }}</option>
              @empty
                  <option disabled>Please add accounts</option>
              @endforelse
            </select>
            @error('account')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <!-- <input id="" type="text" class="form-control form-control-lg" name="account[]" list="accounts"> -->
          </div>
          <div class="col-md-2 pl-0 pr-1">
            <input id="" type="number" class="form-control form-control-lg text-right" name="debit[]" value="">
          </div>
          <div class="col-md-2 pl-0">
            <input id="" type="number" class="form-control form-control-lg text-right" name="credit[]" value="">
          </div>
        </div>
      </div>

      <div class="text-right">
        <a href="#" class="text-sm" @click="items += 1">+ Add Line</a>
      </div>
      <x-input type="text" name="narration" />

      <datalist id="accounts">
        @foreach($accounts as $account)
          <option value="{{$account->name}}">
        @endforeach
      </datalist>

      <button type="submit" class="btn btn-primary">{{ __('Save Transaction') }}</button>
    </form>
  </div>
</div>
@endsection
