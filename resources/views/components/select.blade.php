<div class="form-group">
    <label for="{{ $name }}" class="form-label">{{ __(ucfirst($name)) }}:</label>
    <select class="form-control @error($name) is-invalid @enderror" name="{{ $name }}" id="{{ $name }}">
    <option selected disabled>Please Select</option>
    @forelse($records as $record)
        <option @if(isset($selected)) @if($selected == $record->id) selected @endif @endif value="{{ $record->id }}">{{ $record->$field }}</option>
    @empty
        <option disabled>Please add {{ $name }} first</option>
    @endforelse
    </select>
    @error($name)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>