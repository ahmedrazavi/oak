<div class="form-group">
    <label for="{{ $name }}" class="form-label">{{ __(ucfirst($name)) }}:</label>

    <input id="{{ $name }}" type="{{ $type }}" class="form-control @error($name) is-invalid @enderror {{ $classes }}" name="{{ $name }}" value="{{ old($name, $editName) }}">
    
    @error($name)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>