<div class="form-group">
    <label for="{{ $name }}" class="form-label">{{ __(ucfirst($name)) }}:</label>

    <div class="input-group mb-3">

        @if(isset($prepend))
            <div class="input-group-prepend">
                <span class="input-group-text">{{ $prepend }}</span>
            </div>
        @endif

        <input id="{{ $name }}" type="{{ $type }}" class="form-control @error($name) is-invalid @enderror {{ $classes }}" name="{{ $name }}" value="{{ old($name, $editName) }}">

        @if(isset($append))
            <div class="input-group-append">
                <span class="input-group-text">{{ $append }}</span>
            </div>
        @endif

        @error($name)
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror

    </div>
</div>