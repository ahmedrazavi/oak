@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('classes.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Create Classes</div>
        <form method="POST" action="{{ route('classes.store') }}">
          @csrf

          <div class="row">
            <div class="col-md-6">
              <x-input type="date" name="date" />
            </div>
            <div class="col-md-6">
              <x-input type="time" name="time" />
            </div>
          </div>
          <x-select name="batch" :records="$batches" field="name" />
          <x-select name="teacher" :records="$teachers" field="name" />
          <x-select name="subject" :records="$subjects" field="name" />
          <x-select name="room" :records="$rooms" field="name" />
          <x-input name="hours" type="number" />
          
          <!-- crazy spagetti code right? hahaha :D -->
          <img src="" onerror="{document.getElementById('hours').setAttribute('step','0.1')}">

          <button type="submit" class="btn btn-primary">{{ __('Save Classes') }}</button>
        </form>
      </div>
    </div>
</div>
@endsection
