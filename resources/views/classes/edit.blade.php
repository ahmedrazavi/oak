@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('classes.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Edit Class</div>
        <form method="POST" action="{{ route('classes.update', $classes) }}">
          @csrf @method('PUT')

          <div class="row">
            <div class="col-md-6">
              <x-input type="date" name="date" :editName="date('Y-m-d', strtotime($classes->timming))" />
            </div>
            <div class="col-md-6">
              <x-input type="time" name="time" :editName="date('H:i', strtotime($classes->timming))" />
            </div>
          </div>
          <x-select name="batch" :records="$batches" field="name" :selected="$classes->batch_id" />
          <x-select name="teacher" :records="$teachers" field="name" :selected="$classes->teacher_id"/>
          <x-select name="subject" :records="$subjects" field="name" :selected="$classes->subject_id"/>
          <x-select name="room" :records="$rooms" field="name" :selected="$classes->room_id"/>
          <x-input name="hours" type="number" :editName="$classes->hours"/>
          <div class="form-group">
            <label for="status" class="form-label">Status:</label>
            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status">
              <option @if($classes->status == 'scheduled') selected @endif value="schduled">Scheduled</option>
              <option @if($classes->status == 'posponed') selected @endif value="posponed">Posponed</option>
              <option @if($classes->status == 'conducted') selected @endif value="conducted">Conducted</option>
            </select>
            @error('status')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          
          <!-- crazy spagetti code right? hahaha :D -->
          <img src="" onerror="{document.getElementById('hours').setAttribute('step','0.1')}">

          <button type="submit" class="btn btn-primary">{{ __('Update Class') }}</button>
          <a href="#" onclick="document.getElementById('deleteForm').submit(); return false;" class="btn btn-outline-danger">Delete</a>
        </form>
        <form id="deleteForm" action="{{route('classes.destroy', $classes)}}" method="post">
          @csrf @method('DELETE')
        </form>
      </div>
    </div>
</div>
@endsection
