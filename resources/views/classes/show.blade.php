@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('classes.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body pb-0">
        <div class="card-title h3">{{ $classes->name }}</div>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item d-flex justify-content-between align-items-center">
        ID
        <span>#{{ $classes->id }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Batch
        <span>{{ $classes->batch->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Teacher
        <span>{{ $classes->teacher->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Subject
        <span>{{ $classes->subject->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Room
        <span>{{ $classes->room->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Date
        <span>{{ date('d M Y', strtotime($classes->timming)) }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Time
        <span>{{ date('h:i A', strtotime($classes->timming)) }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Status
        <span class="text-uppercase">{{ $classes->status }}</span>
      </li>
    </ul>
    <div class="m-3 d-flex">
      <form action="{{route('classes.destroy', $classes)}}" method="post">
        @csrf @method('DELETE')
        <a href="{{route('classes.edit',$classes)}}" class="btn btn-primary">Edit Class</a>
        <button type="submit" class="btn btn-outline-danger">Delete</button>
      </form>
    </div>
    
</div>
@endsection
