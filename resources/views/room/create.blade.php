@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('room.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Create Room</div>
        <form method="POST" action="{{ route('room.store') }}">
          @csrf

          <x-input type="text" name="name" />

          <button type="submit" class="btn btn-primary">{{ __('Save Room') }}</button>
        </form>
      </div>
    </div>
</div>
@endsection
