@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('room.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body pb-0">
        <div class="card-title h3">{{ $room->name }}</div>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item d-flex justify-content-between align-items-center">
        ID
        <span>#{{ $room->id }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Name
        <span>{{ $room->name }}</span>
      </li>
    </ul>
    <div class="m-3 d-flex">
      <form action="{{route('room.destroy', $room)}}" method="post">
        @csrf @method('DELETE')
        <a href="{{route('room.edit',$room)}}" class="btn btn-primary">Edit Room</a>
        <button type="submit" class="btn btn-outline-danger">Delete</button>
      </form>
    </div>
    
</div>
@endsection
