@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('room.aside')
      </nav>
    </div>
</div>
<div class="col-md-9">
    @include('includes.message')

    <div class="card">
        <div class="card-body pb-0">
          <div class="d-flex justify-content-between align-items-start">
            <div class="card-title h3">Rooms</div>
            <a href="{{ route('room.create') }}" class="btn btn-sm btn-primary">New</a>
          </div>
        </div>

        <table class="table">
        
          <tr>
            <th>Name</th>
          </tr>

          @forelse($rooms as $room)

            <tr>
              <td><a href="{{ route('room.show', $room->id) }}">{{$room->name}}</a></td>
            </tr>

          @empty
          
            <tr>
              <td colspan="1">No rooms found</td>
            </tr>

          @endforelse

        </table>
    </div>
</div>
@endsection
