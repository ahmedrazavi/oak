@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('room.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Edit Room</div>
        <form method="POST" action="{{ route('room.update', $room) }}">
          @csrf @method('PUT')

          <x-input type="text" name="name" :editName="$room->name" />

          <button type="submit" class="btn btn-primary">{{ __('Update Room') }}</button>
          <a href="#" onclick="document.getElementById('deleteForm').submit(); return false;" class="btn btn-outline-danger">Delete</a>
        </form>
        <form id="deleteForm" action="{{route('room.destroy', $room)}}" method="post">
          @csrf @method('DELETE')
        </form>
      </div>
    </div>
</div>
@endsection
