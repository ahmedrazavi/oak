@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('subject.aside')
      </nav>
    </div>
</div>
<div class="col-md-9">
    @include('includes.message')

    <div class="card">
        <div class="card-body pb-0">
          <div class="card-title h3">Subjects</div>
        </div>

        <table class="table">
        
          <tr>
            <th>Name</th>
            <th>Course Name</th>
            <th>Credit Hours</th>
          </tr>

          @forelse($subjects as $subject)

            <tr>
              <td><a href="{{ route('subject.show', $subject->id) }}">{{$subject->name}}</a></td>
              <td>{{ $subject->course->name }}</td>
              <td>{{ $subject->credit_hours }}</td>
            </tr>

          @empty
          
            <tr>
              <td colspan="4">No Subjects found</td>
            </tr>

          @endforelse

        </table>
    </div>
</div>
@endsection
