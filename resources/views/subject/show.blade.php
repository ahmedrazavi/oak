@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('subject.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body pb-0">
        <div class="card-title h3">{{ $subject->name }}</div>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item d-flex justify-content-between align-items-center">
        ID
        <span>#{{ $subject->id }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Name
        <span>{{ $subject->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Course
        <span>{{ $subject->course->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Credit Hours
        <span>{{ $subject->credit_hours }}</span>
      </li>
    </ul>
    <div class="m-3 d-flex">
      <form action="{{route('subject.destroy', $subject)}}" method="post">
        @csrf @method('DELETE')
        <a href="{{route('subject.edit',$subject)}}" class="btn btn-primary">Edit Subject</a>
        <button type="submit" class="btn btn-outline-danger">Delete</button>
      </form>
    </div>
    
</div>
@endsection
