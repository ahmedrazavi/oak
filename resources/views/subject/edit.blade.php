@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('subject.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Edit Subject</div>
        <form method="POST" action="{{ route('subject.update', $subject) }}">
          @csrf @method('PUT')

          <x-input type="text" name="name" :editName="$subject->name" />

          <div class="form-group">
            <label for="course" class="form-label">Course:</label>

            <select class="form-control @error('course') is-invalid @enderror" name="course" id="course">
              @forelse($courses as $course)
                <option value="{{ $course->id }}" @if($subject->course_id == $course->id) selected @endif>{{ $course->name }}</option>
              @empty
                <option selected disabled>Please add course first</option>
              @endforelse
            </select>

            @error('course')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>

          <x-input type="number" name="credit_hours" :editName="$subject->credit_hours" />

          <button type="submit" class="btn btn-primary">{{ __('Update Subject') }}</button>
          <a href="#" onclick="document.getElementById('deleteForm').submit(); return false;" class="btn btn-outline-danger">Delete</a>
        </form>
        <form id="deleteForm" action="{{route('subject.destroy', $subject)}}" method="post">
          @csrf @method('DELETE')
        </form>
      </div>
    </div>
</div>
@endsection
