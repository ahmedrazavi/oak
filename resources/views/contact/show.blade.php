@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('contact.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body pb-0">
        <div class="card-title h3">{{ $contact->name }}</div>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item d-flex justify-content-between align-items-center">
        ID
        <span>#{{ $contact->id }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Name
        <span>{{ $contact->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Email
        <span>{{ $contact->email }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Phone
        <span>{{ $contact->phone }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Address
        <span>{{ $contact->address }}</span>
      </li>
    </ul>
    <div class="m-3 d-flex">
      <form action="{{route('contact.destroy', $contact)}}" method="post">
        @csrf @method('DELETE')
        <a href="{{route('contact.edit',$contact)}}" class="btn btn-primary">Edit Contact</a>
        <button type="submit" class="btn btn-outline-danger">Delete</button>
      </form>
    </div>
    
</div>
@endsection
