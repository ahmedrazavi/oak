@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('contact.aside')
      </nav>
    </div>
</div>
<div class="col-md-9">
    @include('includes.message')

    <div class="card">
        <div class="card-body pb-0">
          <div class="card-title h3">Contacts</div>
        </div>

        <table class="table">
        
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
          </tr>

          @forelse($contacts as $contact)

            <tr>
              <td><a href="{{ route('contact.show', $contact->id) }}">{{$contact->name}}</a></td>
              <td>{{$contact->email}}</td>
              <td>{{$contact->phone}}</td>
              <td>{{$contact->address}}</td>
            </tr>

          @empty
          
            <tr>
              <td colspan="4">No contacts found</td>
            </tr>

          @endforelse

        </table>
    </div>
</div>
@endsection
