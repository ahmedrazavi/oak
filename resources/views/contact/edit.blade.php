@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('contact.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Edit Contact</div>
        <form method="POST" action="{{ route('contact.update', $contact) }}">
          @csrf @method('PUT')

          <x-input type="text" name="name" :editName="$contact->name" />
          <x-input type="email" name="email" :editName="$contact->email" />
          <x-input type="phone" name="phone" :editName="$contact->phone" />
          <x-input type="text" name="address" :editName="$contact->address" />

          <button type="submit" class="btn btn-primary">{{ __('Update Contact') }}</button>
          <a href="#" onclick="document.getElementById('deleteForm').submit()" class="btn btn-outline-danger">Delete</a>
        </form>
        <form id="deleteForm" action="{{route('contact.destroy', $contact)}}" method="post">
          @csrf @method('DELETE')
        </form>
      </div>
    </div>
</div>
@endsection
