@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('course.aside')
      </nav>
    </div>
</div>
<div class="col-md-9">
    @include('includes.message')

    <div class="card">
        <div class="card-body pb-0">
          <div class="card-title h3">Courses</div>
        </div>

        <table class="table">
        
          <tr>
            <th>Name</th>
            <th class="text-right">Enrolled</th>
            <th class="text-right">Fee</th>
          </tr>

          @forelse($courses as $course)

            <tr>
              <td><a href="{{ route('course.show', $course->id) }}">{{$course->name}}</a></td>
              <td class="text-right">33</td>
              <td class="text-right">Rs. {{$course->fee}}</td>
            </tr>

          @empty
          
            <tr>
              <td colspan="4">No courses found</td>
            </tr>

          @endforelse

        </table>
    </div>
</div>
@endsection
