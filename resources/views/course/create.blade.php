@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('course.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Create Course</div>
        <form method="POST" action="{{ route('course.store') }}">
          @csrf

          <x-input type="text" name="name" />
          <x-input-group type="text" name="fee" classes="text-right" prepend="Rs." append=".00"/>

          <button type="submit" class="btn btn-primary">{{ __('Save Course') }}</button>
        </form>
      </div>
    </div>
</div>
@endsection
