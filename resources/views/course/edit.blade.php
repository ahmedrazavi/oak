@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('course.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body">
        <div class="card-title h3">Edit Course</div>
        <form method="POST" action="{{ route('course.update', $course) }}">
          @csrf @method('PUT')

          <x-input type="text" name="name" :editName="$course->name" />
          <x-input-group type="text" name="fee" :editName="$course->fee" classes="text-right" prepend="Rs." append=".00"/>

          <button type="submit" class="btn btn-primary">{{ __('Update Course') }}</button>
          <a href="#" onclick="document.getElementById('deleteForm').submit(); return false;" class="btn btn-outline-danger">Delete</a>
        </form>
        <form id="deleteForm" action="{{route('course.destroy', $course)}}" method="post">
          @csrf @method('DELETE')
        </form>
      </div>
    </div>
</div>
@endsection
