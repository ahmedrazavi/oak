@extends('layouts.page')

@section('content')
<div class="col-md-3">
  <div class="card">
      <nav class="nav flex-column">
        @include('course.aside')
      </nav>
    </div>
</div>
<div class="col-md-6">
    @include('includes.message')

    <div class="card">
      <div class="card-body pb-0">
        <div class="card-title h3">{{ $course->name }}</div>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item d-flex justify-content-between align-items-center">
        ID
        <span>#{{ $course->id }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Name
        <span>{{ $course->name }}</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Enrolled
        <span>23</span>
      </li>
      <li class="list-group-item d-flex justify-content-between align-items-center">
        Fee
        <span>{{ $course->fee }}</span>
      </li>
    </ul>
    <div class="m-3 d-flex">
      <form action="{{route('course.destroy', $course)}}" method="post">
        @csrf @method('DELETE')
        <a href="{{route('course.edit',$course)}}" class="btn btn-primary">Edit Course</a>
        <button type="submit" class="btn btn-outline-danger">Delete</button>
      </form>
    </div>
    
</div>
@endsection
