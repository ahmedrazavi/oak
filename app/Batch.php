<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    public $timestamps = false;
    protected $fillable = ['course_id', 'session'];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function getNameAttribute()
    {
        return $this->course->name."-".$this->session;
    }
}
