<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $timestamps = false;
    protected $fillable = ['date', 'narration', 'transactionable_type', 'transactionable_id'];
    protected $dates = ['date'];

    public function data()
    {
        return $this->hasMany(TransactionData::class);
    }
}
