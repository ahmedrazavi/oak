<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $timestamps = false;
    protected $fillable = ['nature', 'category', 'control', 'name'];

    public function transactions()
    {
        return $this->hasMany(TransactionData::class);
    }

    public function getBalanceAttribute()
    {
        return $this->transactions->sum('amount');
    }
}
