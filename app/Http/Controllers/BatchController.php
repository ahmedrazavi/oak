<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Course;
use Illuminate\Http\Request;
use App\Http\Requests\StoreBatch;
use App\Http\Requests\UpdateBatch;

class BatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('batch.index', [
            'batches' => Batch::with('course')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('batch.create',[
            'courses' => Course::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBatch $request)
    {
        $batch = Batch::create(array_merge($request->validated(),['course_id' => $request->course]));

        return redirect()->back()->withMessage('Batch Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function show(Batch $batch)
    {
        return view('batch.show', [
            'batch' => $batch
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function edit(Batch $batch)
    {
        return view('batch.edit', [
            'batch' => $batch,
            'courses' => Course::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBatch $request, Batch $batch)
    {
        $batch->update(array_merge($request->validated(),['course_id' => $request->course]));

        return redirect()->back()->withMessage('Batch Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Batch $batch)
    {
        $batch->delete();

        return redirect()->route('batch.index');
    }
}
