<?php

namespace App\Http\Controllers;

use Artisan;
use Illuminate\Http\Request;

class WebArtisanController extends Controller
{
    public function index()
    {
        return view('artisan');
    }

    public function run(Request $request)
    {
        // if (!\App::environment('local')) { throw new AccessDeniedHttpException(); }

        $artisan_output = '';
        $command = $request->command;
        $args = (isset($request->args)) ? ' '.$args : '';

        try {
            Artisan::call($command . $args);
            $artisan_output = Artisan::output();
        } catch (Exception $e) {
            $artisan_output = $e->getMessage();
        }

        return view('artisan', [
            'artisan_output' => $artisan_output
        ]);
    }
}
