<?php

namespace App\Http\Controllers;

use App\Guardian;
use Illuminate\Http\Request;
use App\Http\Requests\StoreGuardian;
use App\Http\Requests\UpdateGuardian;

class GuardianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('guardian.index', [
            'guardians' => Guardian::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('guardian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGuardian $request)
    {
        $guardian = Guardian::create($request->validated());
        $guardian->contact()->create($request->validated());

        return redirect()->back()->withMessage('Guardian Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Guardian  $guardian
     * @return \Illuminate\Http\Response
     */
    public function show(Guardian $guardian)
    {
        return view('guardian.show', [
            'guardian' => $guardian
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Guardian  $guardian
     * @return \Illuminate\Http\Response
     */
    public function edit(Guardian $guardian)
    {
        return view('guardian.edit', [
            'guardian' => $guardian
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Guardian  $guardian
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGuardian $request, Guardian $guardian)
    {
        $guardian->update($request->all());
        $guardian->contact()->update($request->only(['name', 'email', 'phone', 'address']));

        return redirect()->back()->withMessage('Guardian Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Guardian  $guardian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Guardian $guardian)
    {
        $guardian->contact()->delete();
        $guardian->delete();

        return redirect()->route('guardian.index');
    }
}
