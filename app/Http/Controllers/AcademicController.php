<?php

namespace App\Http\Controllers;

use App\Course;
use App\Classes;
use App\Student;
use App\Applicant;
use Illuminate\Http\Request;

class AcademicController extends Controller
{
    public function index()
    {
        return view('academic.index',[
            'classes' => Classes::whereDate('timming',date('Y-m-d'))->get(),
            'applicants' => Applicant::whereStatus('open')->get(),
            'students' => Student::all(),
        ]);
    }

    public function classes()
    {
        return view('academic.classes',[
            'classes' => Classes::all()
        ]);
    }

    public function applicants()
    {
        return view('academic.applicants',[
            'applicants' => Applicant::all()
        ]);
    }

    public function courses()
    {
        return view('academic.courses',[
            'courses' => Course::with('subjects')->get()
        ]);
    }
}
