<?php

namespace App\Http\Controllers;

use App\Classes;
use App\Batch;
use App\Teacher;
use App\Subject;
use App\Room;
use Illuminate\Http\Request;
use App\Http\Requests\StoreClasses;
use App\Http\Requests\UpdateClasses;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('classes.index', [
            'classes' => Classes::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('classes.create',[
            'batches' => Batch::all(),
            'teachers' => Teacher::all(),
            'subjects' => Subject::all(),
            'rooms' => Room::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClasses $request)
    {
        $classes = Classes::create(array_merge($request->validated(),[
            'timming' => date('Y-m-d H:i:s', strtotime($request->date.$request->time)),
            'batch_id' => $request->batch, 
            'teacher_id' => $request->teacher, 
            'subject_id' => $request->subject, 
            'room_id' => $request->room, 
            'status' => 'scheduled'
        ]));

        return redirect()->back()->withMessage('Classes Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function show(Classes $class)
    {
        return view('classes.show', [
            'classes' => $class
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function edit(Classes $class)
    {
        return view('classes.edit', [
            'classes' => $class,
            'batches' => Batch::all(),
            'teachers' => Teacher::all(),
            'subjects' => Subject::all(),
            'rooms' => Room::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClasses $request, Classes $class)
    {
        $class->update(array_merge($request->validated(),[
            'timming' => date('Y-m-d H:i:s', strtotime($request->date.$request->time)),
            'batch_id' => $request->batch, 
            'teacher_id' => $request->teacher, 
            'subject_id' => $request->subject, 
            'room_id' => $request->room, 
            'status' => $request->status
        ]));

        return redirect()->back()->withMessage('Classes Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classes $class)
    {
        $class->delete();

        return redirect()->route('classes.index');
    }
}
