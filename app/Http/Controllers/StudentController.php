<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Student;
use App\Guardian;
use Illuminate\Http\Request;
use App\Http\Requests\StoreStudent;
use App\Http\Requests\UpdateStudent;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('student.index', [
            'students' => Student::all(),
            'batches' => Batch::all()
        ]);
    }

    public function search(Request $request)
    {
        $request->validate([
            'id' => 'numeric|nullable',
            'batch' => 'numeric'
        ]);

        // return redirect()->route('student.index');
        return view('student.index', [
            'students' => Student::whereId($request->id)->orWhere('batch_id',$request->batch)->get(),
            'batches' => Batch::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.create',[
            'guardians' => Guardian::all(),
            'batches' => Batch::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStudent $request)
    {
        $student = Student::create(array_merge($request->validated(),['guardian_id' => $request->guardian, 'batch_id' => $request->batch]));
        $student->contact()->create($request->validated());

        return redirect()->back()->withMessage('Student Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view('student.show', [
            'student' => $student
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('student.edit', [
            'student' => $student,
            'guardians' => Guardian::all(),
            'batches' => Batch::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudent $request, Student $student)
    {
        $student->update(array_merge($request->validated(),['guardian_id' => $request->guardian, 'batch_id' => $request->batch]));
        $student->contact()->update($request->only(['name', 'email', 'phone', 'address']));

        return redirect()->back()->withMessage('Student Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->contact()->delete();
        $student->delete();

        return redirect()->route('student.index');
    }
}
