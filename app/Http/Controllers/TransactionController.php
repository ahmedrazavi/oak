<?php

namespace App\Http\Controllers;

use App\Account;
use App\Transaction;
use App\TransactionData;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTransaction;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('transaction.index',[
            'transactions' => Transaction::with('data')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaction.create',[
            'accounts' => Account::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransaction $request)
    {
        if (array_sum($request->debit) != array_sum($request->credit)) {
            return redirect()->back()->withDanger('Transaction is not balanced');
        }
        
        $transaction = Transaction::create($request->all());

        for ($i=0; $i < count($request->account); $i++) { 
            $transaction->data()->create([
                // 'account_id' => Account::whereName($request->account[$i])->first()->id,
                'account_id' => $request->account[$i],
                'amount' => $request->debit[$i] - $request->credit[$i],
            ]);
        }

        return redirect()->back()->withMessage('Transaction Recorded');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
