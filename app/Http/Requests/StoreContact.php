<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'string|email|max:255|unique:contacts|nullable',
            'phone' => 'regex:/(0)[0-9]{10}/|nullable',
            'address' => 'string|max:255|nullable'
        ];
    }

    public function messages()
    {
        return [
            'phone.regex' => 'A number should be in 0XXXXXXXXXX format',
        ];
    }
}
