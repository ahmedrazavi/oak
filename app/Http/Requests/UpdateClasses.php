<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClasses extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'time' => 'required|date_format:H:i',
            'batch' => 'required|numeric',
            'teacher' => 'required|numeric',
            'subject' => 'required|numeric',
            'room' => 'required|numeric',
            'hours' => 'required|numeric',
            'status' => 'required'
        ];
    }
}
