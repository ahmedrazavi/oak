<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'string|email|max:255|nullable|unique:contacts,email,'.$this->student->contact->id,
            'phone' => 'regex:/(0)[0-9]{10}/|nullable|unique:contacts,phone,'.$this->student->contact->id,
            'address' => 'string|max:255|nullable',
            'gender' => 'string|max:255|nullable',
            'batch' => 'nullable|numeric',
            'guardian' => 'nullable|numeric',
        ];
    }
}
