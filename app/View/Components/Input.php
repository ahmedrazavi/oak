<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    public $type;
    public $name;
    public $editName;
    public $classes;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($type, $name, $editName = NULL, $classes = NULL)
    {
        $this->type = $type;
        $this->name = $name;
        $this->editName = $editName;
        $this->classes = $classes;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.input');
    }
}
