<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public $timestamps = false;
    protected $fillable = ['education'];

    public function contact()
    {
        return $this->morphOne(Contact::class, 'contactable');
    }

    public function getNameAttribute()
    {
        return $this->contact->name;
    }

    public function getEmailAttribute()
    {
        return $this->contact->email;
    }

    public function getPhoneAttribute()
    {
        return $this->contact->phone;
    }

    public function getAddressAttribute()
    {
        return $this->contact->address;
    }    
}
