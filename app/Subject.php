<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public $timestamps = false;
    protected $fillable = ['course_id', 'name', 'credit_hours'];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
