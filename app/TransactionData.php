<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionData extends Model
{
    public $timestamps = false;
    protected $fillable = ['transaction_id', 'account_id', 'amount'];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function sources()
    {
        return $this
        ->hasMany(TransactionData::class, 'transaction_id', 'transaction_id')
        ->where('id', '!=', $this->id);
    }
}
