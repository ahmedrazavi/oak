<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $timestamps = false;
    protected $fillable = ['gender', 'guardian_id', 'batch_id'];

    public function contact()
    {
        return $this->morphOne(Contact::class, 'contactable');
    }

    public function guardian()
    {
        return $this->belongsTo(Guardian::class);
    }

    public function batch()
    {
        return $this->belongsTo(Batch::class);
    }

    public function getNameAttribute()
    {
        return $this->contact->name;
    }

    public function getEmailAttribute()
    {
        return $this->contact->email;
    }

    public function getPhoneAttribute()
    {
        return $this->contact->phone;
    }

    public function getAddressAttribute()
    {
        return $this->contact->address;
    }    
}
