<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    protected $fillable = ['name', 'phone', 'course_id', 'message', 'status'];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
